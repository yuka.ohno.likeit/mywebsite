package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.Time;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        
        Time time = new Time();
		String date = time.Now();
		
		//
		request.setAttribute("date", date);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		String user_id = request.getParameter("user_id");
		String user_name = request.getParameter("user_name");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String create_date = request.getParameter("create_date");
		String update_date = request.getParameter("update_date");
		
		 Time time = new Time();
		String date = time.Now();
		

        String password = null;
		
        //入力漏れがあるならフォワード
		if(
		    user_id.equals("") ||
		    user_name.equals("") ||
			password1.equals("") ||
			password2.equals("")
			) 
		{
			request.setAttribute("errMsg", "入力漏れです");
			request.setAttribute("date", date);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
		
		
		if(password1.equals(password2)) {
		  password = password1;
		  model.Code code = new model.Code();
		  password = code.MakeCode(password);
	    } else {
	    	request.setAttribute("errMsg", "パスワード不一致。");
	    	request.setAttribute("date", date);
	    	
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
	    }
		
		UserDao userDao = new UserDao();
	    int i = userDao.InsertBySignUpInfo(user_id, user_name, password, create_date, update_date);
	   
	    if(i == 0) {
	    	request.setAttribute("errMsg", "入力された内容は正しくありません。");
	    	request.setAttribute("date", date);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
	    } else {
	    	response.sendRedirect("SignUp");
	    }
	    }
	}



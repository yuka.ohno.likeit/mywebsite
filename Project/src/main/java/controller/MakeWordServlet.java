package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CreatemywordDao;
import dao.CreateourwordDao;
import model.User;

/**
 * Servlet implementation class MakeWordServlet
 */
@WebServlet("/MakeWordServlet")
public class MakeWordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MakeWordServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		//何語のものなのか
		HttpSession session_language = request.getSession();
		String language_id1 = (String) session_language.getAttribute("language_id");
//		もし言語情報がないなら言語セレクト画面へ
		if(language_id1 == null) {
			response.sendRedirect("SelectLanguageServlet");
			return;
		}

		//何語のものなのか情報を保持しておくlanguage_nameいるかどうかわからないけど
		String language_id = request.getParameter("language_id");
		String language_name = request.getParameter("language_name");
		request.setAttribute("language_id", language_id);
		request.setAttribute("language_name", language_name);

		//ユーザセッション情報無いならログイン画面に戻る
		HttpSession session = request.getSession(true);
		User userInfo = (User) session.getAttribute("user");

		if(userInfo == null) {
			response.sendRedirect("SignUp");
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/makeword.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		//ユーザ情報取得&ユーザのid取得
		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute("user");
		int user_id = user.getId();
		
//		何語のものなのか
		HttpSession session_language = request.getSession();
		String language_id = (String) session_language.getAttribute("language_id");
//		String language_name = (String) session_language.getAttribute("language_name");
		
		//値取得
		String word = request.getParameter("word");
		String meaning = request.getParameter("meaning");
		String example1 = request.getParameter("example1");
		String example2 = request.getParameter("example2");
		String example3 = request.getParameter("example3");
		String mine_or_ours = request.getParameter("note");
		
//		どちらに保存するか選択してない場合フォワード
		if(mine_or_ours == null) {
			request.setAttribute("errMsg", "保存先を選択してください。");
		//あるならフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/makeword.jsp");
			dispatcher.forward(request, response);
		}
		
        //入力漏れチェック
		if(word.equals("") || meaning.equals("") ) {
			request.setAttribute("errMsg", "入力漏れです。");
		//あるならフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/makeword.jsp");
			dispatcher.forward(request, response);
		}
		
//		自分のノートに入れる
		CreatemywordDao createmywordDao = new CreatemywordDao();
		int rs = createmywordDao.InsertNewWord(word, meaning, example1, example2, example3, user_id, language_id);
		
//		登録できたかチェック
		if(rs == 0) {
			request.setAttribute("errMsg", "登録に失敗しました。もう一度入力してください。");
		//登録失敗したならフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/makeword.jsp");
			dispatcher.forward(request, response);
		}
		
//		皆のノートに入れる選択してたらみんなのノートに入れる
		if(mine_or_ours.equals("ours")) {
			CreateourwordDao createourwordDao = new CreateourwordDao();
			createourwordDao.InsertNewWord(word, meaning, example1, example2, example3, user_id, language_id);
		}
//		マイページに戻る
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/mypage.jsp");
		dispatcher.forward(request, response);
	}

}

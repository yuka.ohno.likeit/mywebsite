package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AnseredmywordDao;
import dao.CreateourwordDao;
import model.Anserdmyword;
import model.Createourword;
import model.RandomChoose5;
import model.TableMove;
import model.User;

/**
 * Servlet implementation class OurQuizeServlet
 */
@WebServlet("/OurQuizeServlet")
public class OurQuizeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OurQuizeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    List<Createourword> wordlist = null;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//ユーザセッション情報無いならログイン画面に戻る
		HttpSession session = request.getSession(true);
		User userInfo = (User) session.getAttribute("user");
		
		if(userInfo == null) {
			response.sendRedirect("SignUp");
			return;
		}
		
		System.out.println(userInfo.getId());
		
		//何語のものなのか
		HttpSession session_language = request.getSession();
		String language_id = (String) session_language.getAttribute("language_id");
//		もし言語情報がないなら言語セレクト画面へ
		if(language_id == null) {
			response.sendRedirect("SelectLanguageServlet");
			return;
		}
		
		//正解数カウント
		String correct_anser1 = request.getParameter("correct_anser");
		if(correct_anser1 == null) {
			response.sendRedirect("SelectLanguageServlet");
			return;
		}
		int correct_anser = Integer.parseInt(correct_anser1);
		
//		何回目の単語なのか
		String quize_number1 = request.getParameter("quize_number");
		int quize_number = Integer.parseInt(quize_number1);
		quize_number++;
		
		
//		初めに単語リストすべて取ってきて、5つ以内のリストを作成する
		if(quize_number == 1) {
			
			//皆の単語リストすべて取ってくる
			CreateourwordDao createourwordDao = new CreateourwordDao();
			List<Createourword> list = createourwordDao.findByLanguageid(language_id);
			
			if(list.isEmpty()) {
				System.out.println("Emptyです。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ourwordempty.jsp");
				dispatcher.forward(request, response);
				return;
			}else {
				System.out.println("Emptyじゃないです。");
			}
			
//		ランダムに5個単語を取得
			RandomChoose5 choose5 = new RandomChoose5();
			wordlist = choose5.Random5to1(list);
//		ランダムな問題5個をセッションスコープに入れる
			HttpSession session5 = request.getSession();
			session5.setAttribute("wordlist", wordlist);
		}
		
//		問題数をスコープに入れて管理
		int quizes = wordlist.size();
		session.setAttribute("quizes", quizes);
		
		
//		クイズ1回~5回やったら終わり
		if(quize_number > quizes) {
			request.setAttribute("correct_anser", correct_anser);
			
			//データ移動(問題を取得)
			List<Createourword> wordlist = (List<Createourword>) session.getAttribute("wordlist");
			int user_id = userInfo.getId();
			
			//アンサーテーブル一覧を取得
			AnseredmywordDao anseredmyword = new AnseredmywordDao();
			List<Anserdmyword> anserwordlist= anseredmyword.findByUseridAndLanguageid(user_id, language_id);
			
			
			//アンサーテーブルに同じ単語があるなら登録しない
			for(Createourword word : wordlist) {
				int duplicate = 0;
				
				for(Anserdmyword myword : anserwordlist) {
					if(myword.getWord().equals(word.getWord())){
						duplicate = 1;
					   }
				    }
				if(duplicate == 0) {
					TableMove tablemove = new TableMove();
					tablemove.Movetable2(word, user_id);	 					
				}
			}
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/quizeresult3.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		
		
		
//		ランダムに取得した5つの単語を順番にスコープに送る
		request.setAttribute("word", wordlist.get(quize_number - 1));
		
		request.setAttribute("quize_number", quize_number);

		request.setAttribute("correct_anser", correct_anser);
		
		request.setAttribute("anseredword", 1);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/quize3.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//回答受け取り
		String anser = request.getParameter("word");
		//何回目の回答なのか受け取り
		String quize_number1 = request.getParameter("quize_number");
		//正解数カウント
		String correct_anser1 = request.getParameter("correct_anser");

		//stringをintに直す
		int quize_number = Integer.parseInt(quize_number1);
		int correct_anser = Integer.parseInt(correct_anser1);
		
		//		正誤判定
		boolean judge = true;
		
		
		//答えを持ってくるためにセッションスコープ呼び出す
		HttpSession session = request.getSession(true);
		List<Createourword> wordlist = (List<Createourword>) session.getAttribute("wordlist");
		
		//間違ってたらfalse
		if(wordlist.get(quize_number - 1).getWord().equals(anser)) {
			judge = true;
			correct_anser++;
		
		} else {
			judge = false;
		}
		
		request.setAttribute("word", wordlist.get(quize_number - 1));
		
		
		
		
		if(judge == true) {
			request.setAttribute("Judge", "◎正解◎");
			request.setAttribute("color", "blue");
		} else {
			request.setAttribute("Judge", "✖不正解✖");
			request.setAttribute("color", "red");
		}
		
		request.setAttribute("quize_number", quize_number);
		request.setAttribute("correct_anser", correct_anser);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/quizeanser3.jsp");
		dispatcher.forward(request, response);
	}

}

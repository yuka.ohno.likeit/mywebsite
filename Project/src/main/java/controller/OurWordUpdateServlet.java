package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CreateourwordDao;
import model.Createourword;
import model.User;

/**
 * Servlet implementation class OurWordUpdateServlet
 */
@WebServlet("/OurWordUpdateServlet")
public class OurWordUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OurWordUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//ユーザセッション情報無いならログイン画面に戻る
		HttpSession session = request.getSession(true);
		User userInfo = (User) session.getAttribute("user");
		
		if(userInfo == null) {
			response.sendRedirect("SignUp");
			return;
		}
		
		//何語のものなのか
		HttpSession session_language = request.getSession();
		String language_id = (String) session_language.getAttribute("language_id");
//		もし言語情報がないなら言語セレクト画面へ
		if(language_id == null) {
			response.sendRedirect("SelectLanguageServlet");
			return;
		}
		
        //単語のidを取得
		String word_id = request.getParameter("word_id");
		request.setAttribute("word_id", word_id);
		
		
		//選択された単語をスコープに入れる
		CreateourwordDao createourwordDao = new CreateourwordDao();
		Createourword myword = createourwordDao.findByWordId(word_id);
		request.setAttribute("myword", myword);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/wordupdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

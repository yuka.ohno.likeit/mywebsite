package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.LanguageDao;
import model.Language;
import model.User;

/**
 * Servlet implementation class SelectLanguageServlet
 */
@WebServlet("/SelectLanguageServlet")
public class SelectLanguageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SelectLanguageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        LanguageDao languageDao = new LanguageDao();
		List<Language> LanguageList = languageDao.findAll();

		// ���N�G�X�g�X�R�[�v�Ƀ��[�U�ꗗ�����Z�b�g
		request.setAttribute("LanguageList", LanguageList);

		HttpSession session = request.getSession(true);
		User userInfo = (User) session.getAttribute("user");

		if(userInfo == null) {
			response.sendRedirect("SignUp");
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/selectlanguage.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

}

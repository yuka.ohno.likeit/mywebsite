package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/SignUp")
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUpServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession(true);
		User userInfo = (User) session.getAttribute("user");

		if(userInfo == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signUp.jsp");
			dispatcher.forward(request, response);
		} else {
			response.sendRedirect("SelectLanguageServlet");
		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String user_id = request.getParameter("user_id");
		String password = request.getParameter("password");


        //入力漏れがあるならフォワード
		if(
		    user_id.equals("") ||
			password.equals("")
			)
		{
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signUp.jsp");
			dispatcher.forward(request, response);
		}

		model.Code code = new model.Code();
		password = code.MakeCode(password);

		UserDao userDao = new UserDao();
		User user = userDao.findByLoginInfo(user_id, password);


//		管理者かどうか
		int Jujgement = 0;


		if (user == null) {
		   //
	   	   request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");
		   //
		   RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signUp.jsp");
		   dispatcher.forward(request, response);
		   return;
		}

//
		if(user.getId()== 1) {
			Jujgement = 1;
		}

//
		HttpSession session = request.getSession();
		session.setAttribute("user", user);
		session.setAttribute("Jujgement", Jujgement);

//
		response.sendRedirect("SelectLanguageServlet");
	}

}

package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetail
 */
@WebServlet("/UserDetail")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");

		 //セッションのユーザー情報持ってくる
		 HttpSession privious_session = request.getSession(true);
		 User userInfo = (User) privious_session.getAttribute("user");

		 //もし何もないならログイン画面に飛ぶ
		 if(userInfo == null) {
			response.sendRedirect("SignUp");
			return;
		}

		 //ログイン中のユーザのid
		 int id = userInfo.getId();

		 //もう一度ユーザ情報をidから探しなおす
		 UserDao userDao = new UserDao();
		 User user = userDao.findByUserNumber(id);
		 //同じ名前のものに入れまーす
		 HttpSession session = request.getSession();
		 session.setAttribute("user", user);


		 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdetail.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

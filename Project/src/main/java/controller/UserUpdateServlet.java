package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.Time;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		Time time = new Time();
		String date = time.Now();

		//現在時間取得
		request.setAttribute("date", date);

		HttpSession session = request.getSession(true);
		User userInfo = (User) session.getAttribute("user");

		if(userInfo == null) {
			response.sendRedirect("SignUp");
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String user_name = request.getParameter("user_name");
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String update_date = request.getParameter("update_date");

		String Updatetedpassword = null;

		
//		パスワード暗号化
		model.Code code = new model.Code();
		
		//現在時間取得
		Time time = new Time();
		String date = time.Now();

		if(user_name.equals("") )
		{
			request.setAttribute("errMsg", "ユーザー名を入力して下さい。");
			request.setAttribute("date", date);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
		}

		if(password1.equals(password2)) {
			Updatetedpassword = password1;
			Updatetedpassword = code.MakeCode(Updatetedpassword);
		} else {
			request.setAttribute("errMsg", "パスワードが不一致です。");
			request.setAttribute("date", date);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
		}
		
		if(password1.equals("") && password2.equals("")) {
			Updatetedpassword = null;
		}

		UserDao userDao = new UserDao();
		int i = userDao.Update(id, password, user_name, Updatetedpassword, update_date);

	    if(i == 0) {
	    	request.setAttribute("errMsg", "入力された内容は正しくありません。");
			request.setAttribute("date", date);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
	    }

	    response.sendRedirect("UserDetail");
	}

}



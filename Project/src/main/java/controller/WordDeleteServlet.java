package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AnseredmywordDao;
import dao.CreatemywordDao;
import dao.CreateourwordDao;
import model.User;

/**
 * Servlet implementation class WordDeleteServlet
 */
@WebServlet("/WordDeleteServlet")
public class WordDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WordDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		//ユーザセッション情報無いならログイン画面に戻る
		HttpSession session = request.getSession(true);
		User userInfo = (User) session.getAttribute("user");
				
		if(userInfo == null) {
			response.sendRedirect("SignUp");
			return;
		}
				
		//何語のものなのか
		HttpSession session_language = request.getSession();
		String language_id = (String) session_language.getAttribute("language_id");
//		もし言語情報がないなら言語セレクト画面へ
		if(language_id == null) {
			response.sendRedirect("SelectLanguageServlet");
			return;
		}
		
		//何番のワードなのかを渡す
		String word_id = request.getParameter("word_id");
		request.setAttribute("word_id", word_id);

		
		//自分のかみんなのか
		String mine_or_ours = request.getParameter("mine_or_ours");
		request.setAttribute("mine_or_ours", mine_or_ours);

		
		//回答済みのものなのか回答済み：１
		String anser = request.getParameter("anser");
		request.setAttribute("anser", anser);

		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/worddelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		//何番のワードなのか取得
		String word_id = request.getParameter("word_id");
		System.out.println(word_id);
		
		//自分のかみんなのか
		String mine_or_ours = request.getParameter("mine_or_ours");
		System.out.println(mine_or_ours);
		
		//回答済みのものなのか回答済み：１
		String anser = request.getParameter("anser");
		System.out.println(anser);

		if(anser.equals("1")) {
			AnseredmywordDao anseredmywordDao = new AnseredmywordDao();
			anseredmywordDao.Delete(word_id);
			//リストにリダイレクト
			response.sendRedirect("AnseredWordListServlet");
			return;
		}
		
		if(mine_or_ours.equals("mine")) {
			CreatemywordDao createmywordDao = new CreatemywordDao();
			createmywordDao.Delete(word_id);			
		} else if(mine_or_ours.equals("ours")) {
			CreateourwordDao createourwordDao = new CreateourwordDao();
			createourwordDao.Delete(word_id);
		}
		
		
		//リストにリダイレクト
		response.sendRedirect("WordListServlet");
	}

}

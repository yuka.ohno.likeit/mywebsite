package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AnseredmywordDao;
import dao.CreatemywordDao;
import dao.CreateourwordDao;
import model.Anserdmyword;
import model.Createmyword;
import model.Createourword;
import model.User;

/**
 * Servlet implementation class WordDetailServlet
 */
@WebServlet("/WordDetailServlet")
public class WordDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WordDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		//ユーザセッション情報無いならログイン画面に戻る
		HttpSession session = request.getSession(true);
		User userInfo = (User) session.getAttribute("user");
		
		if(userInfo == null) {
			response.sendRedirect("SignUp");
			return;
		}
		
		//何語のものなのか
		HttpSession session_language = request.getSession();
		String language_id = (String) session_language.getAttribute("language_id");
//		もし言語情報がないなら言語セレクト画面へ
		if(language_id == null) {
			response.sendRedirect("SelectLanguageServlet");
			return;
		}
		
        //単語のidを取得
		String word_id = request.getParameter("word_id");

        //自分のかみんなのか
		String mine_or_ours = request.getParameter("mine_or_ours");
		request.setAttribute("mine_or_ours", mine_or_ours);
		
		//回答済みのものなのか回答済み：１
		String anser = request.getParameter("anser");
		request.setAttribute("anser", anser);

		//ログイン中のユーザのid
		int id = userInfo.getId();
		
		
		if(anser.equals("1")){
			//登録されてる単語の一覧をスコープに入れる
			AnseredmywordDao anseredmywordDao = new AnseredmywordDao();
			List<Anserdmyword> mywordlist = anseredmywordDao.findByUseridAndLanguageid(id, language_id);
			request.setAttribute("mywordlist", mywordlist);
			//選択された単語をスコープに入れる
			Anserdmyword myword = anseredmywordDao.findByWordId(word_id);
			request.setAttribute("myword", myword);
			
			//メッセージを送る
			request.setAttribute("mes", "回答済み");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/worddetail2.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		if(mine_or_ours.equals("mine")) {
			//登録されてる単語の一覧をスコープに入れる
			CreatemywordDao createmywordDao = new CreatemywordDao();
			List<Createmyword> mywordlist = createmywordDao.findByUseridAndLanguageid(id, language_id);
			request.setAttribute("mywordlist", mywordlist);
			//選択された単語をスコープに入れる
			Createmyword myword = createmywordDao.findByWordId(word_id);
			request.setAttribute("myword", myword);
			//メッセージを送る
			request.setAttribute("mes", "登録済み");
			
		}else if(mine_or_ours.equals("ours")) {
			//登録されてる単語の一覧をスコープに入れる
			CreateourwordDao createourwordDao = new CreateourwordDao();
			List<Createourword> mywordlist = createourwordDao.findByLanguageid(language_id);
			request.setAttribute("mywordlist", mywordlist);
			//選択された単語をスコープに入れる
			Createourword myword = createourwordDao.findByWordId(word_id);
			request.setAttribute("myword", myword);
			//メッセージを送る
			request.setAttribute("mes", "みんなの");
		}
		

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/worddetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

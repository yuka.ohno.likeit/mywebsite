package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CreatemywordDao;
import dao.CreateourwordDao;
import model.Createmyword;
import model.Createourword;
import model.User;

/**
 * Servlet implementation class WordListServlet
 */
@WebServlet("/WordListServlet")
public class WordListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public WordListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//ユーザセッション情報無いならログイン画面に戻る
		HttpSession session = request.getSession(true);
		User userInfo = (User) session.getAttribute("user");
		
		if(userInfo == null) {
			response.sendRedirect("SignUp");
			return;
		}
		
		//何語のものなのか
		HttpSession session_language = request.getSession();
		String language_id = (String) session_language.getAttribute("language_id");
//		もし言語情報がないなら言語セレクト画面へ
		if(language_id == null) {
			response.sendRedirect("SelectLanguageServlet");
			return;
		}
		

		//ログイン中のユーザのid
		int id = userInfo.getId();
		
		//自分のかみんなのか
		String mine_or_ours = (String) session.getAttribute("mine_or_ours");
		String mine_or_ours1 = request.getParameter("mine_or_ours");
		//新たにクリックされたものをスコープに入れる
		if(mine_or_ours1 != null) {
			mine_or_ours = mine_or_ours1;
		}
		HttpSession session_mine_or_ours = request.getSession();
		session_mine_or_ours.setAttribute("mine_or_ours", mine_or_ours);
//		自分のなのかみんなのなのかスコープに入れとく
		request.setAttribute("mine_or_ours", mine_or_ours);
		
		if(mine_or_ours.equals("mine")) {
			//登録されてる自分単語の一覧をスコープに入れる
			CreatemywordDao createmywordDao = new CreatemywordDao();
			List<Createmyword> wordlist = createmywordDao.findByUseridAndLanguageid(id, language_id);
//			スコープに入れてみる
			session.setAttribute("wordlist", wordlist);
			request.setAttribute("ours", 1);
			//メッセージ表示
			request.setAttribute("mes", "登録済み");
		}else if(mine_or_ours.equals("ours")) {
			//登録されてるみんなの単語の一覧をスコープに入れる
			CreateourwordDao createourwordDao = new CreateourwordDao();
			List<Createourword> wordlist = createourwordDao.findByLanguageid(language_id);
			session.setAttribute("wordlist", wordlist);
			//メッセージ表示
			request.setAttribute("mes", "みんなの");
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/wordlist1.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

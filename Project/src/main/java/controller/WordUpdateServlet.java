package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AnseredmywordDao;
import dao.CreatemywordDao;
import dao.CreateourwordDao;
import model.Anserdmyword;
import model.Createmyword;
import model.Createourword;
import model.User;

/**
 * Servlet implementation class WordUpdateServlet
 */
@WebServlet("/WordUpdateServlet")
public class WordUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WordUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//ユーザセッション情報無いならログイン画面に戻る
		HttpSession session = request.getSession(true);
		User userInfo = (User) session.getAttribute("user");
		
		if(userInfo == null) {
			response.sendRedirect("SignUp");
			return;
		}
		
		//何語のものなのか
		HttpSession session_language = request.getSession();
		String language_id = (String) session_language.getAttribute("language_id");
//		もし言語情報がないなら言語セレクト画面へ
		if(language_id == null) {
			response.sendRedirect("SelectLanguageServlet");
			return;
		}
		
        //単語のidを取得
		String word_id = request.getParameter("word_id");
		request.setAttribute("word_id", word_id);
		
//		自分のかみんなのか
		String mine_or_ours = request.getParameter("mine_or_ours");
		request.setAttribute("mine_or_ours", mine_or_ours);
		
		//回答済みのものなのか回答済み：１
		String anser = request.getParameter("anser");
		request.setAttribute("anser", anser);
		
		if(anser.equals("1")){
			AnseredmywordDao anserdmywordDao = new AnseredmywordDao();
			Anserdmyword word = anserdmywordDao.findByWordId(word_id);
			request.setAttribute("word", word);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/wordupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		if(mine_or_ours.equals("mine")) {
			//選択された単語をスコープに入れる
			CreatemywordDao createmywordDao = new CreatemywordDao();
			Createmyword word = createmywordDao.findByWordId(word_id);
			request.setAttribute("word", word);
		}else if(mine_or_ours.equals("ours")) {
			//選択された単語をスコープに入れる
			CreateourwordDao createourwordDao = new CreateourwordDao();
			Createourword word = createourwordDao.findByWordId(word_id);
			request.setAttribute("word", word);
		}
		
		

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/wordupdate.jsp");
		dispatcher.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
        //自分のかみんなのか
		String mine_or_ours = request.getParameter("mine_or_ours");
		//回答済みのものなのか回答済み：１
		String anser = request.getParameter("anser");
		//更新する値を持ってくる
		String word_id = request.getParameter("word_id");
		String word1 = request.getParameter("word");
		String meaning = request.getParameter("meaning");
		String example1 = request.getParameter("example1");
		String example2 = request.getParameter("example2");
		String example3 = request.getParameter("example3");
		int rs = 0;
		
		
		if(anser.equals("1")){
			AnseredmywordDao anseredmywordDao = new AnseredmywordDao();
			rs = anseredmywordDao.Update(word_id, word1, meaning, example1, example2, example3);
			mine_or_ours = "nothing";
		}
		
		if(mine_or_ours.equals("mine")) {
			//単語更新のSQL
			CreatemywordDao createmywordDao = new CreatemywordDao();
			rs = createmywordDao.Update(word_id, word1, meaning, example1, example2, example3);
			
		}else if(mine_or_ours.equals("ours")) {
			//単語更新のSQL
			CreateourwordDao createourwordDao = new CreateourwordDao();
			rs = createourwordDao.Update(word_id, word1, meaning, example1, example2, example3);
			
		}
		
		//保存に失敗した場合
		if(rs == 0) {
//			メッセージ表示
			request.setAttribute("errMsg", "登録に失敗しました。");
//			今までの情報をスコープに入れる
			request.setAttribute("word_id", word_id);
			
			//選択された単語をスコープに入れる
			if(anser.equals("1")){
				AnseredmywordDao anseredmywordDao = new AnseredmywordDao();
				Anserdmyword word = anseredmywordDao.findByWordId(word_id);
				request.setAttribute("word", word);
			}else if(mine_or_ours.equals("mine")) {
				//選択された単語をスコープに入れる
				CreatemywordDao createmywordDao = new CreatemywordDao();
				Createmyword word = createmywordDao.findByWordId(word_id);
				request.setAttribute("word", word);
			}else if(mine_or_ours.equals("ours")) {
				//選択された単語をスコープに入れる
				CreateourwordDao createourwordDao = new CreateourwordDao();
				Createourword word = createourwordDao.findByWordId(word_id);
				request.setAttribute("word", word);
			}
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/wordupdate.jsp");
			dispatcher.forward(request, response);
		}
		
		if(anser.equals("1")){
			response.sendRedirect("AnseredWordListServlet");
			return;
		}
//		ワードリストにリダイレクト
		response.sendRedirect("WordListServlet");
	}

}

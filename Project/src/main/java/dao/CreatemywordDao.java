package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Createmyword;

public class CreatemywordDao {

	public List<Createmyword> findByUseridAndLanguageid(int user_id, String language_id){
		Connection con = null;
        List<Createmyword> createmywordlist = new ArrayList<Createmyword>();

        try {
    	    //
    	    con = DBManager.getConnection();

//    	    SQL��
    	    String sql = "SELECT * FROM createmyword WHERE user_id = ? AND language = ?";

//    	    �X�e�[�g�����g�ɓ����
    	    PreparedStatement pstmt = con.prepareStatement(sql);
    	    pstmt.setInt(1, user_id);
    	    pstmt.setString(2, language_id);

//    	    ����
    	    ResultSet rs = pstmt.executeQuery();


    	    while(rs.next()) {
    	    	int id = rs.getInt("id");
                String word = rs.getString("word");
                String meaning = rs.getString("meaning");
                String example1 = rs.getString("example1");
                String example2 = rs.getString("example2");
                String example3 = rs.getString("example3");
                int user_id1 = rs.getInt("user_id");
                int language = rs.getInt("language");
                Createmyword createmyword = new Createmyword(id, word, meaning, example1, example2, example3, user_id1, language);

                createmywordlist.add(createmyword);
    	    }

        } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
	 // �f�[�^�x�[�X�ؒf
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return createmywordlist;
}
	
	
	public Createmyword findByWordId(String word_id){
		Connection con = null;
		
		int id = 0;
        String word = null;
        String meaning = null;
        String example1 = null;
        String example2 = null;
        String example3 = null;
        int user_id1 = 0;
        int language = 0;

        try {
    	    //
    	    con = DBManager.getConnection();

//    	    SQL��
    	    String sql = "SELECT * FROM createmyword WHERE id=?";

//    	    �X�e�[�g�����g�ɓ����
    	    PreparedStatement pstmt = con.prepareStatement(sql);
    	    pstmt.setString(1, word_id);

//    	    ����
    	    ResultSet rs = pstmt.executeQuery();
    	    
    	    if(!rs.next()) {
    	    	return null;
    	    }

    	    id = rs.getInt("id");
            word = rs.getString("word");
            meaning = rs.getString("meaning");
            example1 = rs.getString("example1");
            example2 = rs.getString("example2");
            example3 = rs.getString("example3");
            user_id1 = rs.getInt("user_id");
            language = rs.getInt("language");
            
            
        } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
	 // �f�[�^�x�[�X�ؒf
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return new Createmyword(id, word, meaning, example1, example2, example3, user_id1, language);
}
	
	
	public int InsertNewWord(String word, String meaning, String example1, String example2, String example3, int user_id, String language_id) {
		Connection con = null;
		int rs = 0;

	    try {
	    con = DBManager.getConnection();


	    String sql = "INSERT INTO createmyword(word, meaning, example1, example2, example3, user_id, language) VALUES(?, ?, ?, ?, ?, ?, ?)";

	    PreparedStatement pstmt = con.prepareStatement(sql);
	    
	    pstmt.setString(1, word);
	    pstmt.setString(2, meaning);
	    pstmt.setString(3, example1);
	    pstmt.setString(4, example2);
	    pstmt.setString(5, example3);
	    pstmt.setInt(6, user_id);
	    pstmt.setString(7, language_id);


	    rs = pstmt.executeUpdate();

	    } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return rs;
	}
	
	
	public int Update(String word_id, String word, String meaning, String example1, String example2, String example3) {
		Connection con = null;
		int rs = 0;

	    try {
	    // データベース接続
	    con = DBManager.getConnection();


	    String sql = "UPDATE createmyword SET word=?, meaning=?, example1=?, example2=?, example3=? WHERE id=?";


	    PreparedStatement pstmt = con.prepareStatement(sql);

        pstmt.setString(1, word);
	    pstmt.setString(2, meaning);
	    pstmt.setString(3, example1);
	    pstmt.setString(4, example2);
	    pstmt.setString(5, example3);
	    pstmt.setString(6, word_id);

	    rs = pstmt.executeUpdate();

	    } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return rs;
	}
	
	
	public int Delete(String word_id) {
		Connection con = null;
		int rs = 0;

	    try {
	    // �f�[�^�x�[�X�֐ڑ�
	    con = DBManager.getConnection();

//	    SQL��
	    String sql = "DELETE FROM createmyword WHERE id=?";

//	    �X�e�[�g�����g�ɓ����
	    PreparedStatement pstmt = con.prepareStatement(sql);
	    pstmt.setString(1, word_id);

//	    ����
	    rs = pstmt.executeUpdate();

	    } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
	 // �f�[�^�x�[�X�ؒf
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return rs;
	}

}

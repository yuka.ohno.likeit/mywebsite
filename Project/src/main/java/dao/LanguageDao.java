package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Language;

public class LanguageDao {
	
	public List<Language> findAll(){
		Connection con = null;
        List<Language> langageList = new ArrayList<Language>();
        
        try {
    	    con = DBManager.getConnection();
    	    
//    	    SQL��
    	    String sql = "SELECT * FROM language";
    	    
//    	    SELECT�������s�����ʕ[���擾
    	    Statement st = con.createStatement();
    	    ResultSet rs = st.executeQuery(sql);
    	    
    	    while(rs.next()) {
    	    	int id = rs.getInt("id");
                String language = rs.getString("language");
                Language Language = new Language(id, language);
                
                langageList.add(Language);
    	    }
        } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
	 // �f�[�^�x�[�X�ؒf
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return langageList;
}
	
}

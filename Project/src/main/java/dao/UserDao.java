package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.mysql.jdbc.StringUtils;

import model.User;

public class UserDao {

	public int InsertBySignUpInfo(String user_id, String user_name, String password, String create_date, String update_date) {
		Connection con = null;
		int rs = 0;

	    try {
	    con = DBManager.getConnection();


	    String sql = "INSERT INTO user(user_id, user_name, password, created_date, updated_date) VALUES(?, ?, ?, ?, ?)";

	    PreparedStatement pstmt = con.prepareStatement(sql);
	    pstmt.setString(1, user_id);
	    pstmt.setString(2, user_name);
	    pstmt.setString(3, password);
	    pstmt.setString(4, create_date);
	    pstmt.setString(5, update_date);


	    rs = pstmt.executeUpdate();

	    } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return rs;
	}


	public User findByLoginInfo(String user_id, String password) {
		int idData = 0;
		String user_idData = null;
		String user_nameData = null;
		String passwordData = null;
		Date createDateData = null;
		Date updateDateData = null;

		Connection con = null;

	    try {
	    // �f�[�^�x�[�X�֐ڑ�
	    con = DBManager.getConnection();

//	    SQL��
	    String sql = "SELECT * FROM user WHERE user_id = ? AND password = ?";

//	    �X�e�[�g�����g�ɓ����
	    PreparedStatement pstmt = con.prepareStatement(sql);
	    pstmt.setString(1, user_id);
	    pstmt.setString(2, password);

//	    ����
	    ResultSet rs = pstmt.executeQuery();

//	    �v�f����
	    if(!rs.next()) {
	    	return null;
	    }

//	    �J����������l�Q�b�g����user�N���X�̃t�B�[���h�ɓ��ꂽ�̂�߂�
	    idData = rs.getInt("id");
	    user_idData = rs.getString("user_id");
	    user_nameData = rs.getString("user_name");
	    passwordData = rs.getString("password");
	    createDateData = rs.getTimestamp("created_date");
	    updateDateData = rs.getTimestamp("updated_date");


	   } catch (SQLException e) {
	        	e.printStackTrace();
	   } finally {
		 // �f�[�^�x�[�X�ؒf
        if (con != null) {
        	try {
              		con.close();
          	} catch (SQLException e) {
              		e.printStackTrace();
          	}
      	}
	   }
		return new User(idData, user_idData, user_nameData, passwordData, createDateData, updateDateData);
	}

	public User findByUserNumber(int id) {
		int idData = 0;
		String user_idData = null;
		String user_nameData = null;
		String passwordData = null;
		Date createDateData = null;
		Date updateDateData = null;

		Connection con = null;

	    try {
	    // �f�[�^�x�[�X�֐ڑ�
	    con = DBManager.getConnection();

//	    SQL��
	    String sql = "SELECT * FROM user WHERE id=?";

//	    �X�e�[�g�����g�ɓ����
	    PreparedStatement pstmt = con.prepareStatement(sql);
	    pstmt.setInt(1, id);

//	    ����
	    ResultSet rs = pstmt.executeQuery();

//	    �v�f����
	    if(!rs.next()) {
	    	return null;
	    }

//	    �J����������l�Q�b�g����user�N���X�̃t�B�[���h�ɓ��ꂽ�̂�߂�
	    idData = rs.getInt("id");
	    user_idData = rs.getString("user_id");
	    user_nameData = rs.getString("user_name");
	    passwordData = rs.getString("password");
	    createDateData = rs.getTimestamp("created_date");
	    updateDateData = rs.getTimestamp("updated_date");


	   } catch (SQLException e) {
	        	e.printStackTrace();
	   } finally {
		 // �f�[�^�x�[�X�ؒf
        if (con != null) {
        	try {
              		con.close();
          	} catch (SQLException e) {
              		e.printStackTrace();
          	}
      	}
	   }
		return new User(idData, user_idData, user_nameData, passwordData, createDateData, updateDateData);
	}



	public int Update(String id, String password, String user_name, String Updatetedpassword, String update_date) {
		Connection con = null;
		int rs = 0;

	    try {
	    // データベース接続
	    con = DBManager.getConnection();


	    String sql = "UPDATE user SET password=?, user_name=?, updated_date=? WHERE id=?;";


	    PreparedStatement pstmt = con.prepareStatement(sql);

          if(StringUtils.isNullOrEmpty(Updatetedpassword)) {
        	  pstmt.setString(1, password);
          } else {
        	  pstmt.setString(1, Updatetedpassword);
          }
	    pstmt.setString(2, user_name);
	    pstmt.setString(3, update_date);
	    pstmt.setString(4, id);

	    rs = pstmt.executeUpdate();

	    } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return rs;
	}


	public int Delete(String id) {
		Connection con = null;
		int rs = 0;

	    try {
	    // �f�[�^�x�[�X�֐ڑ�
	    con = DBManager.getConnection();

//	    SQL��
	    String sql = "DELETE FROM user WHERE id = ?";

//	    �X�e�[�g�����g�ɓ����
	    PreparedStatement pstmt = con.prepareStatement(sql);
	    pstmt.setString(1, id);

//	    ����
	    rs = pstmt.executeUpdate();

	    } catch (SQLException e) {
        	e.printStackTrace();
   } finally {
	 // �f�[�^�x�[�X�ؒf
    if (con != null) {
    	try {
          		con.close();
      	} catch (SQLException e) {
          		e.printStackTrace();
      	}
  	}
   }
		return rs;
	}

}

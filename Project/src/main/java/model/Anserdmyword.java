package model;

import java.io.Serializable;

public class Anserdmyword implements Serializable{
	private int id;
	private String word;
	private String meaning;
	private String example1;
	private String example2;
	private String example3;
	private int user_id;
	private int language;
	private int myword_id;
	private int ourword_id;
	
	public Anserdmyword() {
	}

	public Anserdmyword(int id, String word, String meaning, String example1, String example2, String example3,
			int user_id, int language, int myword_id, int ourword_id) {
		super();
		this.id = id;
		this.word = word;
		this.meaning = meaning;
		this.example1 = example1;
		this.example2 = example2;
		this.example3 = example3;
		this.user_id = user_id;
		this.language = language;
		this.myword_id = myword_id;
		this.ourword_id = ourword_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getMeaning() {
		return meaning;
	}

	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}

	public String getExample1() {
		return example1;
	}

	public void setExample1(String example1) {
		this.example1 = example1;
	}

	public String getExample2() {
		return example2;
	}

	public void setExample2(String example2) {
		this.example2 = example2;
	}

	public String getExample3() {
		return example3;
	}

	public void setExample3(String example3) {
		this.example3 = example3;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getLanguage() {
		return language;
	}

	public void setLanguage(int language) {
		this.language = language;
	}

	public int getMyword_id() {
		return myword_id;
	}

	public void setMyword_id(int myword_id) {
		this.myword_id = myword_id;
	}

	public int getOurword_id() {
		return ourword_id;
	}

	public void setOurword_id(int ourword_id) {
		this.ourword_id = ourword_id;
	}
	
	
}

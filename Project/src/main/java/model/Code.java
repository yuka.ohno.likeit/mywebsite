package model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Code {
	
	public String MakeCode(String plainText) {

		// ②MessageDigestのインスタンスを生成
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		// ③文字列のバイト列をupdate
		md.update(plainText.getBytes());

		// ④変換後バイト列を取得
		byte[] hashBytes = md.digest();

		// ⑤16進数に変えつつ桁数を補正
		int[] hashInts = new int[hashBytes.length];
		StringBuilder sb = new StringBuilder();
		for (int i=0; i < hashBytes.length; i++) {
			hashInts[i] = (int)hashBytes[i] & 0xff;
			if (hashInts[i] <= 15) {
				sb.append("0");
			}
			sb.append(Integer.toHexString(hashInts[i]));
		}

		// ⑥標準出力
		String hashText = sb.toString();
		return hashText;
	}
	
}

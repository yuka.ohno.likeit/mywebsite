package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandomChoose5 {
	
	public  List<Createmyword> Random5(List<Createmyword> wordlist){
		List<Createmyword> list = new ArrayList<Createmyword>();
		
		// リストが何個入ってるのか
		int size = wordlist.size();
		
		//中身の要素をシャッフルする
		Collections.shuffle(wordlist);
		
		if(size < 5) {
			for(Createmyword word : wordlist) {
				list.add(word);
			}
		}else {
			for(int i = 0; i < 5; i++) {
				list.add(wordlist.get(i));
			}
		}
		
		return list;
	}
	
	public  List<Createourword> Random(List<Createourword> wordlist){
		List<Createourword> list = new ArrayList<Createourword>();
		
		// リストが何個入ってるのか
		int size = wordlist.size();
		
		//中身の要素をシャッフルする
		Collections.shuffle(wordlist);
		
		if(size < 5) {
			for(Createourword word : wordlist) {
				list.add(word);
			}
		}else {
			for(int i = 0; i < 5; i++) {
				list.add(wordlist.get(i));
			}
		}
		
		return list;
	}
	
	public  List<Createourword> Random5to1(List<Createourword> wordlist){
		List<Createourword> list = new ArrayList<Createourword>();;
		
		// リストが何個入ってるのか
		int size = wordlist.size();
		
		//中身の要素をシャッフルする
		Collections.shuffle(wordlist);
		
		if(size < 5) {
			for(Createourword word : wordlist) {
				list.add(word);
			}
		}else {
			for(int i = 0; i < 5; i++) {
				list.add(wordlist.get(i));
			}
		}
		
		return list;
	}
	
	public  List<Anserdmyword> Random1to5(List<Anserdmyword> wordlist){
		List<Anserdmyword> list = new ArrayList<Anserdmyword>();;
		
		// リストが何個入ってるのか
		int size = wordlist.size();
		
		//中身の要素をシャッフルする
		Collections.shuffle(wordlist);
		
		if(size < 5) {
			for(Anserdmyword word : wordlist) {
				list.add(word);
			}
		}else {
			for(int i = 0; i < 5; i++) {
				list.add(wordlist.get(i));
			}
		}
		
		return list;
	}
}

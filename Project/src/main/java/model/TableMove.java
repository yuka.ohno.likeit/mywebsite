package model;

import dao.AnseredmywordDao;
import dao.CreatemywordDao;

public class TableMove {
	public void Movetable(Createmyword word, int user_id){
		int id1 = word.getId();
		String id = Integer.toString(id1);
        String word1 = word.getWord();
        String meaning = word.getMeaning();
        String example1 = word.getExample1();
        String example2 = word.getExample2();
        String example3 = word.getExample3();
        //問題を解いた人
        int user_id1 = user_id;
        int language = word.getLanguage();
        //問題作成者
        int myword_id = word.getUser_id();
        
        //アンサーマイワードに入れる
        AnseredmywordDao anseredmyword = new AnseredmywordDao();
        int rs1 = anseredmyword.InsertNewWord(word1, meaning, example1, example2, example3, user_id1, language, myword_id);
        if(rs1 == 0) {
        	System.out.println("登録失敗。");
        }
        
        //元のテーブルから削除する
        CreatemywordDao createmywordDao = new CreatemywordDao();
        int rs2 = createmywordDao.Delete(id);
        if(rs2 == 0) {
        	System.out.println("削除失敗。");
        }
	}
	
	public void Movetable2(Createourword word, int user_id){
		int id1 = word.getId();
		String id = Integer.toString(id1);
        String word1 = word.getWord();
        String meaning = word.getMeaning();
        String example1 = word.getExample1();
        String example2 = word.getExample2();
        String example3 = word.getExample3();
        //問題を解いた人
        int user_id1 = user_id;
        int language = word.getLanguage();
        //問題作成者
        int myword_id = word.getUser_id();
        
        //アンサーテーブルに入れる
        AnseredmywordDao anseredmyword = new AnseredmywordDao();        
        int rs1 = anseredmyword.InsertNewWord(word1, meaning, example1, example2, example3, user_id1, language, myword_id);
        if(rs1 == 0) {
        	System.out.println("登録失敗。");
        }
	}
}

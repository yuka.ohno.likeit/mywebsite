package model;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private int id;
	private String user_id;
	private String user_name;
	private String password;
	private Date create_date;
	private Date update_date;
	
	public User() {
	}
	
	public User(int id, String user_id, String user_name, String password, Date create_date, Date update_date) {
		super();
		this.id = id;
		this.user_id = user_id;
		this.user_name = user_name;
		this.password = password;
		this.create_date = create_date;
		this.update_date = update_date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
	
	
}

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>makeword</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
<link rel="stylesheet" href="makeword.css">
</head>

<header>
    <ul>
        <li><a href="LogoutServlet" class="lisa">ログアウト</a></li>
        <li><a href="UserDetail" class="lisa">ユーザ情報</a></li>
        <li><a href="SelectLanguageServlet" class="lisa">TOP画面</a></li>
        <li>ようこそ${user.user_name}さん</li>
    </ul>
</header>

<body>
    <div class="wa">
        <div class="blockquote text-center">
       　　<h1>単語登録</h1>
        
           　<div class="loginwarapper">
               <form action="MakeWordServlet" method="POST">
                
                   <div class="yoko">
                       <p>単語</p>
                       <input type="text" name="word">
                   </div>
                   <div class="yoko">
                       <p>意味</p>
                       <input type="text" name="meaning">
                   </div>
                   <div class="colums">
                       <p>例文</p>
                       <input type="text" name="example1">
                       <input type="text" name="example2">
                       <input type="text" name="example3">
                   </div>
                   <div>
                       <p id="hidden" class="hidden">みんなのノートとに保存するとほかの人があなたさんが保存した
                        単語を見れるようになります。</p>
                    </div>
                    <div>
                        <p class="quistion" style="margin-left: 120px;">？皆のノートとは</p>
                    </div>
                    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
                    <script> 
                    document.getElementById('hidden').style.visibility = 'hidden';
                       $('.quistion').on('click', function() {
                           document.getElementById('hidden').style.visibility = 'visible';
                    });
                    </script>
                   <div class="radio">
                   <input type="radio" id="mine" name="note" value="mine">
                   <label for="mine">自分のノートに保存</label>

                   <input type="radio" id="mine" name="note" value="ours">
                   <label for="mine">自分＆みんなのノートに保存</label>
                   </div>
                    <button type="submit" class="btn btn-primary button">登録</button>   
                    
                    <a href="MypageServlet?language_id=${language_id}&language_name=${language_name}" id="back" class="back">戻る</a>
                </form>
                
                <p>	<c:if test="${errMsg != null}" >
                  <div class="alert alert-danger al" role="alert">
                    ${errMsg}
                  </div>
              </c:if></p>
                
            </div>
           
        　　　</div>
    　　</div>
    </div>

       
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>mypage</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
<link rel="stylesheet" href="top.css">
</head>

<header>
    <ul>
        <li><a href="LogoutServlet" class="lisa">ログアウト</a></li>
        <li><a href="UserDetail" class="lisa">ユーザ情報</a></li>
        <li><a href="SelectLanguageServlet" class="lisa">TOP画面</a></li>
        <li>ようこそ${user.user_name}さん</li>
    </ul>
</header>

<body>
    <div class="wa">
        <div class="blockquote text-center">
       <h1>みんなの${language_name}ノート</h1>

           <div class="loginwarapper">
               <div class="languagewarapper">
                   <div class="wordresister">
                       <a href="MakeWordServlet" class="langa">単語を登録する</a>
                   </div>
                   <div class="wordresister">
                       <a href="OurQuizeServlet?quize_number=0&correct_anser=0" class="langa">皆のノートからクイズを解く</a>
                   </div>

                   <div class="flex">
                       <div class="others">
                        <a href="WordListServlet?mine_or_ours=mine" class="langa">単語ノート</a>
                    </div>

                    <div class="others">
                        <a href="AnseredWordListServlet" class="langa">回答済み単語ノート</a>
                    </div>
                    <c:if test="${user.id == 1}" >
                        <div class="others">
                        <a href="WordListServlet?mine_or_ours=ours" class="langa">みんなの単語ノート</a>
                        </div>
                    </c:if>

                   </div>

               </div>
         </div>
      </div>
      </div>


    </body>
    </html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>quize</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
<link rel="stylesheet" href="quize.css">
</head>

<header>
    <ul>
        <li><a href="LogoutServlet" class="lisa">ログアウト</a></li>
        <li><a href="UserDetail" class="lisa">ユーザ情報</a></li>
        <li><a href="SelectLanguageServlet" class="lisa">TOP画面</a></li>
        <li>ようこそ${user.user_name}さん</li>
    </ul>
</header>

<body>
    <div class="wa">
        <div class="blockquote text-center">
           　<div class="loginwarapper">
               <form action="QuizeServlet?quize_number=${quize_number}&correct_anser=${correct_anser}" method="POST">
                   <div class="number">
                       <p>${quize_number}
                       /${quizes}</p>
                   </div>
                   <div class="yoko">
                       <p>単語</p>
                       <input type="text" name="word">
                   </div>
                   <div class="yoko">
                       <p>意味</p>
                       <p>${word.meaning}</p>
                   </div>
                   
                   
                    <button type="submit" class="btn btn-primary button">回答</button>   
                    
                </form>
            </div>
           
        　　　</div>
    　　</div>
    </div>

       
</body>
</html>
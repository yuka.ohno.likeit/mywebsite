<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>quizeanser</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
<link rel="stylesheet" href="quizeanser.css">
</head>

<header>
    <ul>
        <li><a href="LogoutServlet" class="lisa">ログアウト</a></li>
        <li><a href="UserDetail" class="lisa">ユーザ情報</a></li>
        <li><a href="SelectLanguageServlet" class="lisa">TOP画面</a></li>
        <li>ようこそ${user.user_name}さん</li>
    </ul>
</header>

<body>
    <div class="wa">
        <div class="blockquote text-center">

           　<div class="loginwarapper">
                   <div class="alert alert-danger al" role="alert" style="background-color: ${color};">
                    ${Judge}
                  </div>
                   <div class="yoko">
                       <p>単語</p>
                       <p>${word.word}</p>
                   </div>
                   <div class="yoko">
                       <p>意味</p>
                       <p>${word.meaning}</p>
                   </div>
                   <div class="colums">
                       <p>例文</p>
                      <p>${word.example1}</p>
                      <c:if test="${word.example1.isEmpty()}">
                      <p>-未入力-</p>
                       </c:if>
                      <p>${word.example2}</p>
                      <c:if test="${word.example2.isEmpty()}">
                      <p>-未入力-</p>
                       </c:if>
                      <p>${word.example3}</p>
                      <c:if test="${word.example3.isEmpty()}">
                      <p>-未入力-</p>
                       </c:if>
                   </div>
                   
                    <a href="QuizeServlet?quize_number=${quize_number}&correct_anser=${correct_anser}" class="btn btn-primary button">次へ</a>   

            </div>
           
        </div>
    </div>

       
</body>
</html>
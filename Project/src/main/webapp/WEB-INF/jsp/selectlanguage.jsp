<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>selectlanguage</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
<link rel="stylesheet" href="selectlanguage.css">
</head>

<header>
    <ul>
        <li><a href="LogoutServlet" class="lisa">ログアウト</a></li>
        <li><a href="UserDetail" class="lisa">ユーザ情報</a></li>
        <li><a href="SelectLanguageServlet" class="lisa">TOP画面</a></li>
        <li>ようこそ${user.user_name}さん</li>
    </ul>
</header>

<body>
    <div class="wa">
        <div class="blockquote text-center">
       <h1>みんなの外国語ノート</h1>
       <h3>皆でノートをシェアする単語帳<br>
        単語を登録してクイズに答えて覚えよう</h3>

           <div class="loginwarapper">
               <div class="languagewarapper">

          <c:forEach var="languages" items="${LanguageList}" >
                   <div class="languages">
                       <a href="MypageServlet?language_id=${languages.id}&language_name=${languages.language}" class="langa">${languages.language}</a>
                   </div>

                </c:forEach>

               </div>
           </div>
    </div>
    </div>


    </body>
    </html>
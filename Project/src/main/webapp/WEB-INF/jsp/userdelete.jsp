<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userdelete</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
<link rel="stylesheet" href="userdelete.css">
</head>

<header>
    <ul>
        <li><a href="#" class="lisa">ログアウト</a></li>
        <li><a href="#" class="lisa">ユーザ情報</a></li>
        <li><a href="#" class="lisa">TOP画面</a></li>
        <li>ようこそ${user.user_name}さん</li>
    </ul>
</header>

<body>
    <div class="wa">
        <div class="blockquote text-center">
       <h1>ユーザ情報</h1>
        
           <div class="loginwarapper">
               <div class="username">
                   <p>ユーザ名</p>
                   <p>${user.user_name}</p>
               </div>
               <div class="confirme">
                   <p>本当に削除しますか？</p>
               </div>
            <div class="row detail">
            <form action="UserDetail">
            <button type="submit" class="btn btn-primary button back">戻る</button>   
            </form>
            <form action="UserDeleteServlet?id=${user.id}" method="post">
            <button type="submit" class="btn btn-primary button delete">削除</button>   
            </form>
        　　</div>
        </div>
    </div>
    </div>

       
    </body>
    </html>
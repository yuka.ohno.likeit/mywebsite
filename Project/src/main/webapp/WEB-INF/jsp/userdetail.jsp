<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:useBean id="date" class="java.util.Date"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userdetail</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
<link rel="stylesheet" href="userdetail.css">
</head>

<header>
    <ul>
        <li><a href="LogoutServlet" class="lisa">ログアウト</a></li>
        <li><a href="UserDetail" class="lisa">ユーザ情報</a></li>
        <li><a href="SelectLanguageServlet" class="lisa">TOP画面</a></li>
        <li>ようこそ${user.user_name}さん</li>
    </ul>
</header>

<body>
    <div class="wa">
        <div class="blockquote text-center">
       <h1>ユーザ情報</h1>

           <div class="loginwarapper">
               <div class="row detail">
                   <p>ユーザ名</p>
                   <p>${user.user_name}</p>
               </div>
               <div class="row detail">
                   <p>ユーザID</p>
                   <p>${user.user_id}</p>
               </div>
               <div class="row detail">
                <p>登録日時</p>
                <p><fmt:formatDate value="${user.create_date}" pattern="yyyy年MM月dd日HH:mm" /></p>
               </div>
               <div class="row detail">
                <p>更新日時</p>
                <p><p><fmt:formatDate value="${user.update_date}" pattern="yyyy年MM月dd日HH:mm" /></p>
                </div>

                <a href="UserUpdateServlet" class="btn btn-primary button">更新</a>
                <a href="UserDeleteServlet" class="btn btn-primary button">アカウント削除</a>
           </div>
        </div>
    </div>


</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
<link rel="stylesheet" href="login.css">
</head>
<header>
    <ul>
        <li><a href="LogoutServlet" class="lisa">ログアウト</a></li>
        <li><a href="UserDetail" class="lisa">ユーザ情報</a></li>
        <li><a href="SelectLanguageServlet" class="lisa">TOP画面</a></li>
        <li>ようこそ${user.user_name}さん</li>
    </ul>
</header>
<body>

    <div class="blockquote text-center">

            <div class="loginwarapper">
                <div class="logo">
                    <p>ユーザ情報更新</p>
                </div>
                <div class="blockquote text-center">
                <form action="UserUpdateServlet?id=${user.id}&password=${user.password}" method="post">
                    <div class="input">
                        <label for="exampleInputusername">　  ユーザ名　　</label>
                        <input type="text" name="user_name" value="${user.user_name}">
                    </div>
                <div class="blockquote text-center">
                    <div class="input row">
                        <label for="exampleInputEmail1">　 ユーザID 　</label>
                        <p style="padding-left: 35px;">${user.user_id}</p>
                    </div>
                    <div class="input">
                        <label for="exampleInputPassword1"> 　パスワード 　</label>
                        <input type="text" name="password1">
                    </div>
                    <div class="input">
                        <label for="exampleInputPassword2">パスワード(確認)</label>
                        <input type="text" name="password2">
                    </div>

                <input type="hidden" name="update_date" value="${date}">


                    <button type="submit" class="btn btn-primary button">登録</button>

                 </div>
                 </form>
                 <p>	<c:if test="${errMsg != null}" >
                  <div class="alert alert-danger al" role="alert">
                    ${errMsg}
                  </div>
              </c:if></p>
            </div>
          </div>
        </div>

    </body>
</html>
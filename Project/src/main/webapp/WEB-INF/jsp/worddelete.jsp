<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>worddeletedelete</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
<link rel="stylesheet" href="worddelete.css">
</head>

<header>
    <ul>
        <li><a href="LogoutServlet" class="lisa">ログアウト</a></li>
        <li><a href="UserDetail" class="lisa">ユーザ情報</a></li>
        <li><a href="SelectLanguageServlet" class="lisa">TOP画面</a></li>
        <li>ようこそ${user.user_name}さん</li>
    </ul>
</header>

<body>
    <div class="wa">
        <div class="blockquote text-center">
       
           <div class="loginwarapper">
               <div class="confirme">
                   <p>本当に削除しますか？</p>
               </div>
            <div class="row detail">
            <form action="WordListServlet">
            <button type="submit" class="btn btn-primary button back">戻る</button>   
            </form>
            <form action="WordDeleteServlet?word_id=${word_id}&mine_or_ours=${mine_or_ours}&anser=${anser}" method="post">
            <button type="submit" class="btn btn-primary button delete">削除</button>   
            </form>
        　　</div>
        </div>
    </div>
</div>
       
</body>
</html>
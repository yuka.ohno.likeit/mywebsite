<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>wordlist</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
<link rel="stylesheet" href="wordlist.css">
</head>

<header>
    <ul>
        <li><a href="LogoutServlet" class="lisa">ログアウト</a></li>
        <li><a href="UserDetail" class="lisa">ユーザ情報</a></li>
        <li><a href="SelectLanguageServlet" class="lisa">TOP画面</a></li>
        <li>ようこそ${user.user_name}さん</li>
    </ul>
</header>

<body>
    <div class="wa">
    　　<div class="blockquote text-center">
       <div class="header">
           <h1>${mes}
           <br>
           単語リスト</h1>
            <c:if test="${!wordlist.isEmpty()}">
             <a href="AnseredQuizeServlet?wordlist=${wordlist}&quize_number=0&correct_anser=0">クイズ</a>
             </c:if>
       </div>　　
        
           <div class="loginwarapper">
           <c:if test="${mywordlist.isEmpty()}">
           <div>単語を登録してください</div>
           <a href="MypageServlet?language_id=${language_id}&language_name=${language_name}">戻る</a>
           </c:if>

           <c:if test="${!mywordlist.isEmpty()}">
            <table border="1">
                <tr>
                  <th class="sth">単語</th>
                  <th class="sth">意味</th>
                  <th class="sth"></th>
                </tr>

            <c:forEach var="word" items="${wordlist}" >
                <tr>
                  <td class="word"><a href="WordDetailServlet?word_id=${word.id}&mine_or_ours=${mine_or_ours}&anser=1" class="bl">${word.word}</a></td>
                  <td class="word"><a href="WordDetailServlet?word_id=${word.id}&mine_or_ours=${mine_or_ours}&anser=1" class="bl">${word.meaning}</a></td>
                  <td><a class="update btn" href="WordUpdateServlet?word_id=${word.id}&mine_or_ours=${mine_or_ours}&anser=1">更新</a><a class="delete btn" href="WordDeleteServlet?word_id=${word.id}&anser=1">削除</a></td>
                </tr>
            </c:forEach>

            </table>
           </c:if>
            <a href="MypageServlet?language_id=${language_id}&language_name=${language_name}" id="back" class="back">戻る</a>
              <div id="worddeletewrap" class="worddeletewrap">
                  <div>
                      <p class="bold">単語</p>
                      <p>${myword.word}</p>
                  </div>
                  <div>
                      <p class="bold">意味</p>
                      <p>${myword.meaning}</p>
                  </div>
                  <div>
                      <p class="bold">例文</p>
                      <p>${myword.example1}</p>
                      <c:if test="${myword.example1.isEmpty()}">
                      <p>-未入力-</p>
                       </c:if>
                      <p>${myword.example2}</p>
                      <c:if test="${myword.example2.isEmpty()}">
                      <p>-未入力-</p>
                       </c:if>
                      <p>${myword.example3}</p>
                      <c:if test="${myword.example3.isEmpty()}">
                      <p>-未入力-</p>
                       </c:if>
                  </div>
                  <a href="#" id="back" class="back">戻る</a>
              </div>

            </div>

            <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
                    <script> 
                       $('.back').on('click', function() {
                        document.getElementById('worddeletewrap').style.visibility = 'hidden';
                        });
                    </script>
           
        </div>
    </div>

       
</body>
</html>
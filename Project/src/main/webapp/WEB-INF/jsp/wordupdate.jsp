<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>updateword</title>
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    crossorigin="anonymous">
<link rel="stylesheet" href="updateword.css">
</head>

<header>
    <ul>
        <li><a href="LogoutServlet" class="lisa">ログアウト</a></li>
        <li><a href="UserDetail" class="lisa">ユーザ情報</a></li>
        <li><a href="SelectLanguageServlet" class="lisa">TOP画面</a></li>
        <li>ようこそ${user.user_name}さん</li>
    </ul>
</header>

<body>
    <div class="wa">
        <div class="blockquote text-center">
       　　<h1>単語更新</h1>
        
           　<div class="loginwarapper">
               <form action="WordUpdateServlet?word_id=${word.id}&mine_or_ours=${mine_or_ours}&anser=${anser}" method="POST">
                
                   <div class="yoko">
                       <p>単語</p>
                       <input type="text" name="word" value="${word.word}">
                   </div>
                   <div class="yoko">
                       <p>意味</p>
                       <input type="text" name="meaning" value="${word.meaning}">
                   </div>
                   <div class="colums">
                       <p>例文</p>
                       <input type="text" name="example1" value="${word.example1}">
                       <input type="text" name="example2" value="${word.example2}">
                       <input type="text" name="example3" value="${word.example3}">
                   </div>
                   
                    <button type="submit" class="btn btn-primary button">更新</button>   
                    
                </form>
            <a href="WordListServlet?mine_or_ours=${mine_or_ours}" id="back" class="back">戻る</a>
             </div>
            <p>	<c:if test="${errMsg != null}" >
                  <div class="alert alert-danger al" role="alert">
                    ${errMsg}
                  </div>
              </c:if></p>
        </div>
    </div>


       
</body>
</html>
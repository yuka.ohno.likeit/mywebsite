﻿CREATE DATABASE myproject DEFAULT CHARACTER SET utf8;
USE myproject;

CREATE TABLE user (
  id int PRIMARY KEY AUTO_INCREMENT,
  user_id varchar(256) NOT NULL,
  user_name varchar(256) NOT NULL,
  password varchar(256) NOT NULL,
  created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
CREATE TABLE createourword (
  id int PRIMARY KEY AUTO_INCREMENT,
  word varchar(256) NOT NULL,
  meaning varchar(256) NOT NULL,
  example1 varchar(256),
  example2 varchar(256),
  example3 varchar(256),
  user_id int NOT NULL,
  language int NOT NULL
);

SELECT * FROM createourword WHERE user_id = 1 AND language = 2;
SELECT * FROM createourword WHERE language=1;

CREATE TABLE createmyword (
  id int PRIMARY KEY AUTO_INCREMENT,
  word varchar(256) NOT NULL,
  meaning varchar(256) NOT NULL,
  example1 varchar(256),
  example2 varchar(256),
  example3 varchar(256),
  user_id int NOT NULL,
  language int NOT NULL
);
UPDATE createmyword SET word='run', meaning='走る', example1='', example2='', example3='' WHERE id=7;

INSERT INTO createmyword(word, meaning, example1, example2, example3, user_id, language) VALUES('do', 'やる', '', '', '', 1, 2);
INSERT INTO createmyword(word, meaning, example1, example2, example3, user_id, language) VALUES('go', '行く', '', '', '', 1, 2);
INSERT INTO createmyword(word, meaning, example1, example2, example3, user_id, language) VALUES('can', '出来る', '', '', '', 1, 2);

DELETE FROM createmyword WHERE id=10;

SELECT * FROM createmyword WHERE user_id = 1 AND language = 2;
SELECT * FROM createmyword WHERE user_id = 1 AND language = '2';
SELECT * FROM createmyword WHERE id=11;

CREATE TABLE anseredmyword (
  id int PRIMARY KEY AUTO_INCREMENT,
  word varchar(256) NOT NULL,
  meaning varchar(256) NOT NULL,
  example1 varchar(256),
  example2 varchar(256),
  example3 varchar(256),
  user_id int NOT NULL,
  language int NOT NULL,
  myword_id int,
  ourword_id int
);
CREATE TABLE language (
  id int NOT NULL UNIQUE,
  language varchar(256) NOT NULL
);
INSERT INTO language VALUES(1, '韓国語');
INSERT INTO language VALUES(2, '英語');
INSERT INTO language VALUES(3, 'フランス語');
INSERT INTO language VALUES(4, 'インドネシア語');
























INSERT INTO user(user_id, user_name, password)
VALUES('sora', 'yuka', 'sora');

SELECT * FROM user WHERE user_id = 'sora' AND password='sora';

UPDATE user SET user_name = 'sorao' WHERE id=1;

SELECT * FROM language;

UPDATE user SET user_id='sorao', user_name='sorao', password='sorao' WHERE id=7;

INSERT INTO user(user_id, user_name, password, created_date, updated_date) VALUES('a', 'a', 'a', '2021-06-16 16:29:35', '2021-06-16 16:29:35');

DROP DATABASE myproject;
DROP TABLE user;
DROP TABLE createourword;
DROP TABLE createmyword;
DROP TABLE anseredmyword;
DROP TABLE language;

